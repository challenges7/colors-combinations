const path = require("path");
const dev = process.env.NODE_ENV === "dev";
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

let config = {
    entry: {
        main: ["./assets/scss/main.scss", "./assets/js/main.js"],
    },
    output: {
        path: path.resolve(process.cwd(), "assets/dist"),
        filename: "js/[name].min.js",
        publicPath: "../",
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
        alias: {
            vue: "vue/dist/vue.esm-bundler.js",
        },
    },
    context: path.resolve(__dirname),
    mode: dev ? "development" : "production",
    watch: dev,
    watchOptions: {
        ignored: /node_modules/,
        poll: 1000,
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)$/,
                type: "asset/resource",
                generator: {
                    filename: "img/[name].[hash:8][ext]",
                },
            },
            {
                test: /\.(ttf|eot|woff2?)$/,
                type: "asset/resource",
                generator: {
                    filename: "fonts/[name].[hash:8][ext]",
                },
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/[name].min.css",
        }),
        new CleanWebpackPlugin(),
    ],
};

module.exports = config;
