// Gsap Config
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger.js";

gsap.registerPlugin(ScrollTrigger);
ScrollTrigger.defaults({
    toggleActions: "restart pause resume none",
    markers: false,
});

// Load
const loader = document.querySelector(".loader");
const loader_balls = loader.querySelector(".balls");
const loader_content = loader.querySelector(".content");
const loader_button = loader_content.querySelector(".button");
const loader_timeline_01 = gsap.timeline();
const loader_timeline_02 = gsap.timeline();
const body = document.querySelector("body");
const header = document.querySelector("header");
const main = document.querySelector("main");
const footer = document.querySelector("footer");

window.addEventListener("load", function () {
    setTimeout(() => {
        loader_timeline_01
            .to(loader_balls, {
                opacity: 0,
                duration: 0.2,
            })
            .to(loader_balls, {
                visibility: "hidden",
                duration: 0,
            })
            .to(loader_content, {
                visibility: "visible",
                duration: 0,
            })
            .to(loader_content, {
                opacity: 1,
                duration: 0.2,
            });
    }, 2000);
});

loader_button.addEventListener("click", () => {
    loader_timeline_02
        .to(loader_content, {
            opacity: 0,
            duration: 0.2,
        })
        .to([header, main, footer], {
            opacity: 1,
            duration: 0,
        })
        .to(body, {
            className: "-=is-freeze",
        })
        .to(loader, {
            yPercent: -150,
            opacity: 0.6,
            duration: 0.4,
        })
        .to(loader, {
            display: "none",
            duration: 0,
        });
});

// Section viewfinder
const viewfinder = document.querySelector(".section--viewfinder");
const viewfinder_lens = viewfinder.querySelector(".lens");
const viewfinder_colors = viewfinder.querySelector(".colors-wrapper");

gsap.to(viewfinder_lens, {
    scrollTrigger: {
        trigger: viewfinder,
        start: "top bottom",
        end: "bottom top",
        scrub: 1,
    },
    rotation: 70,
    ease: "none",
});

gsap.to(viewfinder_colors, {
    scrollTrigger: {
        trigger: viewfinder,
        start: "top bottom",
        end: "bottom top",
        scrub: 1,
    },
    xPercent: -20,
    ease: "none",
});

// Section inter
const inter = document.querySelector(".section--inter");
const inter_words = inter.querySelectorAll(".title span");
const inter_excerpt = inter.querySelector(".excerpt");

inter_words.forEach((word) => {
    gsap.from(word, {
        scrollTrigger: {
            trigger: word,
            start: "top bottom-=100px",
            toggleActions: "play none none none",
        },
        rotation: 7,
        duration: 0.7,
        opacity: 0,
    });
});

gsap.to(inter_excerpt, {
    scrollTrigger: {
        trigger: inter,
        start: "top bottom",
        end: "bottom top",
        scrub: 1,
    },
    yPercent: -180,
    ease: "none",
});

ScrollTrigger.create({
    trigger: inter,
    start: "top 100px",
    onEnter: () => {
        const hex = body.dataset.hex;
        document.documentElement.style.setProperty("--color-02", hex);
    },
    onLeaveBack: () => {
        document.documentElement.style.removeProperty("--color-02");
    },
});

// Section pickers
const picker = document.querySelector(".section--picker");
const picker_lines = document.querySelector(".picker-lines");
const picker_colors = picker.querySelector(".colors");
const picker_colors_items = picker_colors.querySelectorAll(".color");
const picker_top = picker.querySelector(".top");
const picker_bottom = picker.querySelector(".bottom");
const picker_gallery = picker.querySelector(".gallery");
const current_tone = document.querySelector(".current-tone");
const current_hex = document.querySelector(".current-hex");
const current_rgb = document.querySelector(".current-rgb");
const bubble = document.querySelector(".bubble");
const bubble_tl = gsap.timeline();

let number = picker_colors_items[0].dataset.number;
let hex = picker_colors_items[0].dataset.hex;
let tone = picker_colors_items[0].dataset.tone;
let rgb = picker_colors_items[0].dataset.rgb;
const changeColorDatas = () => {
    body.dataset.hex = hex;
    picker_top.dataset.number = number;
    current_tone.innerHTML = tone;
    current_hex.innerHTML = hex;
    current_rgb.innerHTML = rgb;
};
changeColorDatas();

const picket_tl = gsap.timeline();
picket_tl
    .from(picker_lines, {
        xPercent: () => -100,
    })
    .from(
        [picker_top, picker_bottom, picker_gallery],
        {
            opacity: 0,
            duration: 0.5,
        },
        "+=30%"
    )
    .to(
        picker_colors,
        {
            x: 0,
        },
        "-=100%"
    );

const changeColor = (e) => {
    bubble_tl.resume();

    const el = e.target.closest(".color");
    number = el.dataset?.number;
    tone = el.dataset?.tone;
    hex = el.dataset?.hex;
    rgb = el.dataset?.rgb;
    changeColorDatas();

    const x = e.clientX;
    const y = e.clientY;

    bubble_tl
        .to(bubble, {
            top: y,
            left: x,
            background: hex,
            duration: 0,
        })
        .to(bubble, {
            width: () => Math.max(window.innerHeight, window.innerWidth) * 2,
            duration: 0.4,
            onComplete: () => {
                document.documentElement.style.setProperty("--color-02", hex);
            },
        })
        .to(
            bubble,
            {
                width: 0,
                duration: 0,
            },
            "+=0.3"
        );
};

gsap.to(picker, {
    scrollTrigger: {
        trigger: picker,
        start: "center center",
        end: () => "+=" + window.innerHeight,
        scrub: true,
        pin: true,
    },
});

ScrollTrigger.create({
    animation: picket_tl,
    trigger: picker,
    start: "400 bottom",
    end: () => "+=" + (window.innerHeight - 100),
    scrub: true,
    onLeave: () => {
        picker_colors_items.forEach((color) => {
            color.addEventListener("click", changeColor);
        });
    },
    onEnterBack: () => {
        picker_colors_items.forEach((color, i) => {
            color.removeEventListener("click", changeColor);
        });
    },
});
